#!/usr/bin/env node

const DARWIN = 'darwin'

if (process.platform === DARWIN) {
    let msgs = [
        'Since there are some problems with built-in sed in darwin, Please intstall gun-sed width homebrew:',
        'brew install gun-sed',
        'or any way you like'
    ]
    console.log('\n' + msgs.join('\n'));
}
