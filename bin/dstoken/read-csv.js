const path = require('path')
const csv = require('csv');
const fs = require('fs');


const readCSV = async (target) => {
    console.log('Reading file:', target)
    let records = []
    let parser
    try {
        parser = fs
            .createReadStream(target)
            .pipe(csv.parse({
                // CSV options if any
            }));
    } catch {
        console.log('The file does not exist:', target)
        return null
    }

    for await (const record of parser) {
        records.push(record)
    }
    return records
}

module.exports.readCSV = readCSV