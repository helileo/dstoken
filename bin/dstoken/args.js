const readline = require('readline');

let getAnswer = (question) => {
    console.log(question)
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
        prompt: '>: '
    });
    rl.prompt()
    return new Promise((resolve, reject) => {
        rl.on('line', (line) => {
            resolve(line)
            rl.close()
        })
    })
}

module.exports.getAnswer = getAnswer

