const fs = require('fs');


function ge2Digit(num) {
    if (num < 10) {
        return '0' + num
    }
    return '' + num
}

function genFolder(path) {
    try {
        fs.mkdirSync(path, {
            recursive: true
        })
        return path
    } catch (err) {
        console.log('err', err)
        return null
    }
}

function getFolderByDate(date) {
    let fullyear = date.getFullYear()

    let monthIdx = date.getMonth()
    let month = ge2Digit(monthIdx + 1)

    let dateIdx = date.getDate()
    let date2 = ge2Digit(dateIdx)

    let hourIdx = date.getHours()
    let hour = ge2Digit(hourIdx)

    let minuteIdx = date.getMinutes()
    let minute = ge2Digit(minuteIdx)

    let secondIdx = date.getSeconds()
    let second = ge2Digit(secondIdx)
    return `${fullyear}-${month}-${date2}_${hour}_${minute}_${second}`
}

function getLineNo(lineStr) {
    let lineNo = ''
    let lineNoPart = /^ *([^:]+):/
    let res = lineNoPart.exec(lineStr)
    if (res) {
        lineNo = res[1]
    }
    return lineNo
}

function getToken(lineStr) {
    let token = ''
    let tokenPart = /(?<= +)[^ ]+?[ ]*$/
    let res = tokenPart.exec(lineStr)
    if (res) {
        token = res[0].trim()
    }
    return token
}

function getProperty(lineStr) {
    let property = ''
    let propertyPart = /(?<=✖ +)[^"]+(?= +")/
    let res = propertyPart.exec(lineStr)
    if (res) {
        property = res[0]
    }
    return property
}

function getValue(lineStr) {
    let value = ''
    let valuePart = /(?<=").+(?=")/
    let res = valuePart.exec(lineStr)
    if (res) {
        value = res[0]
    }
    return value
}

function isPathLine(lineStr) {
    let isPathLineReg = /(\.{1,2})?\/?(\/[^/])*[A-Za-z0-9-_]+\.[a-zA-Z0-9]{2,}$/
    return isPathLineReg.test(lineStr)
}



module.exports.genFolder = genFolder
module.exports.getFolderByDate = getFolderByDate
module.exports.dismember = {
    isPathLine,
    getToken,
    getLineNo,
    getProperty,
    getValue
}