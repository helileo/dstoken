const stdin = process.stdin;
const { chartset } = require('./conf')

function pipeInput() {
    return new Promise((resolve, reject) => {
        let data = '';

        stdin.setEncoding(chartset);

        stdin.on('data', function (chunk) {
            data += chunk;
        });

        stdin.on('end', function () {
            resolve(data)
        });

        stdin.on('error', reject);

    })
}

module.exports = pipeInput
