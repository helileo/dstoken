#!/bin/bash

lineno=$1
replace=$2
filepath=$3

cmd=${lineno}${replace}${filepath}
echo $cmd >tmp

gsed -i "${lineno}c\\${replace}" ${filepath}
# gsed "${lineno}d" ${filepath}
