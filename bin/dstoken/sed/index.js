
const { execSync } = require('child_process');
const path = require('path')

/**
 * 
 * @param {number} lineNo 
 * @param {string} filePath 
 * @returns {string} 
 */
function getLine(lineNo, filePath) {
    let res, absShellPath
    if (process.platform === 'darwin') {
        absShellPath = path.resolve(__dirname, 'getLineDarwin.sh')
    } else {
        absShellPath = path.resolve(__dirname, 'getLine.sh')
    }

    return execSync(`sh ${absShellPath} ${lineNo} ${filePath}`, { shell: true, encoding: 'utf8' })
}

/**
 * 
 * @param {number} lineNo 
 * @param {string} replace 
 * @param {string} filePath 
 */
function replaceLine(lineNo, replace, filePath) {
    let cmd, absShellPath
    if (process.platform === 'darwin') {
        absShellPath = path.resolve(__dirname, 'replaceLineDarwin.sh')
    } else {
        absShellPath = path.resolve(__dirname, 'replaceLineDarwin.sh')

    }

    cmd = `sh ${absShellPath} ${lineNo} '${replace}' ${filePath}`

    execSync(cmd, { shell: true, encoding: 'utf8' })
}

module.exports.getLine = getLine
module.exports.replaceLine = replaceLine