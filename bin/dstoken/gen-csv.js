const fs = require('fs');
const readline = require('readline');
const csv = require('csv')
const { csvKeys, csvCols, unknowTokens } = require('./conf')
const { dismember } = require('./lib')

async function genCSV(path, saveFolder) {

    let rule = {}
    let currentPATH = ''

    const fileStream = fs.createReadStream(path);
    const rl = readline.createInterface({
        input: fileStream,
        crlfDelay: Infinity
    });
    // 注意：使用 crlfDelay 选项
    // 将 input.txt 中的所有 CR LF ('\r\n') 实例识别为单个换行符。

    for await (const line of rl) {
        // input.txt 中的每一行都将在此处作为 `line` 连续可用。

        if (line.length === 0) {
            continue
        }

        let isPath = dismember.isPathLine(line)
        if (isPath) {
            currentPATH = line
            continue
        }

        let value = dismember.getValue(line)
        let token = dismember.getToken(line)
        let property = dismember.getProperty(line)
        let lineNo = dismember.getLineNo(line)

        if (!token) {
            token = unknowTokens
        }

        if (token && !rule[token]) {
            let header = (function () {
                let header = {}
                csvCols.forEach(one => {
                    header[one.key] = one.name
                })
                return header
            })()
            rule[token] = [header]

        }

        /* need-to-know */
        /* 需要人为确认与csvCols相互匹配 */
        rule[token].push({
            [csvKeys.path.key]: currentPATH,
            [csvKeys.error.key]: line,
            [csvKeys.token.key]: token,
            [csvKeys.property.key]: property,
            [csvKeys.value.key]: value,
            [csvKeys.desc.key]: '',
            [csvKeys.newtoken.key]: '',
            [csvKeys.lineno.key]: lineNo
        })
    }

    Object.keys(rule).forEach(key => {
        csv.stringify(rule[key], {
            columns: csvCols.map(one => {
                return { key: one.key }
            }),
        }, function (err, data) {
            fs.writeFile(`${saveFolder}/${key.substring(key.indexOf('/') + 1)}.csv`, data, err => {
                if (err) {
                    console.error(err)
                    return
                }
                //文件写入成功。
            })
        })
    })
}

module.exports.genCSV = genCSV
