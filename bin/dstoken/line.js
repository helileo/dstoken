const { dismember } = require('./lib');

function processLine(lineStr) {
    if (lineStr.length === 0) {
        return null
    }

    let isPath = dismember.isPathLine(lineStr)
    let token = dismember.getToken(lineStr)
    let property = dismember.getProperty(lineStr)
    let value = dismember.getValue(lineStr)
    let lineNo = dismember.getLineNo(lineStr)

    return {
        isPath,
        token,
        property,
        value,
        lineNo
    }
}

module.exports.processLine = processLine