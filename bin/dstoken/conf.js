// 行属性Fields
const csvKeys = {
    path: {
        key: 'path',
        name: 'Path(开发用)'
    },
    error: {
        key: 'error',
        name: 'Error Msg'
    },
    token: {
        key: 'token',
        name: 'token'
    },
    property: {
        key: 'property',
        name: 'property'
    },
    value: {
        key: 'value',
        name: 'value'
    },
    newtoken: {
        key: 'newtoken',
        name: 'New Token'
    },
    desc: {
        key: 'desc',
        name: '备注'
    },
    lineno: {
        key: 'lineno',
        name: 'Line No'
    }

}

/* need-to-know */
// 表格列,修改此处需要匹配 gen-csv
const csvCols = [
    csvKeys.value,
    csvKeys.error,
    csvKeys.newtoken,
    csvKeys.desc,
    csvKeys.path,
    csvKeys.lineno
]

module.exports.chartset = 'utf8'
module.exports.stylelintErrUmformat = 'stylelint_err_unformat'
module.exports.stylelintErrCsvs = 'stylelint_err_csvs'
module.exports.stylelintCorrectingCsvs = 'stylelint_correcting_csvs'
module.exports.stylelintUnReplaceAbleCsvs = 'stylelint_unreplaceable_csvs'
module.exports.unknowTokens = 'unknow-tokens'
module.exports.csvKeys = csvKeys
module.exports.csvCols = csvCols
