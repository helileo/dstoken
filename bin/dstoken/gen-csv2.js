const fs = require('fs');
const readline = require('readline');
const csv = require('csv')
const { csvKeys, csvCols, unknowTokens } = require('./conf')
const { processLine } = require('./line')

async function genCSV(stream) {

    let rule = {}
    let currentPATH = ''

    // const fileStream = fs.createReadStream(path);
    const rl = readline.createInterface({
        input: stream,
        crlfDelay: Infinity
    });
    // 注意：使用 crlfDelay 选项
    // 将 input.txt 中的所有 CR LF ('\r\n') 实例识别为单个换行符。

    let file = ''

    for await (const line of rl) {
        // input.txt 中的每一行都将在此处作为 `line` 连续可用。
        file += line + '\n'

        let lineData = processLine(line)

        if (!lineData) {
            continue
        }

        let { isPath, token, property, value, lineNo } = lineData

        if (isPath) {
            currentPATH = line
            continue
        }
        if (!token) {
            token = unknowTokens
        }

        if (token && !rule[token]) {
            let header = (function () {
                let header = {}
                csvCols.forEach(one => {
                    header[one.key] = one.name
                })
                return header
            })()
            rule[token] = [header]

        }

        /* need-to-know */
        /* 需要人为确认与csvCols相互匹配 */
        rule[token].push({
            [csvKeys.path.key]: currentPATH,
            [csvKeys.error.key]: line,
            [csvKeys.token.key]: token,
            [csvKeys.property.key]: property,
            [csvKeys.value.key]: value,
            [csvKeys.desc.key]: '',
            [csvKeys.newtoken.key]: '',
            [csvKeys.lineno.key]: lineNo
        })
    }

    rl.close();

    let csvFiles = []

    let ps = []

    Object.keys(rule).forEach(key => {
        ps.push(new Promise((resolve, reject) => {
            csv.stringify(rule[key], {
                columns: csvCols.map(one => {
                    return { key: one.key }
                }),
            }, function (err, data) {
                resolve({
                    csvStr: data,
                    fileName: `${key.substring(key.indexOf('/') + 1)}.csv`
                })
                // fs.writeFile(`${saveFolder}/${key.substring(key.indexOf('/') + 1)}.csv`, data, err => {
                //     if (err) {
                //         console.error(err)
                //         return
                //     }
                //     //文件写入成功。
                // })
            })
        }))
    })

    csvFiles = await Promise.all(ps)

    return {
        file,
        csvFiles
    }
}

module.exports.genCSV = genCSV
