#!/usr/bin/env node

const path = require('path')
const fs = require('fs');

const { genFolder, getFolderByDate } = require('./dstoken/lib.js')
const {
    stylelintErrUmformat,
    stylelintErrCsvs,
    stylelintCorrectingCsvs,
    stylelintUnReplaceAbleCsvs } = require('./dstoken/conf')
const { genCSV } = require('./dstoken/gen-csv2')
const { writeFolder } = require('./dstoken/args')

const stdin = process.stdin;
let [folder] = Array.prototype.splice.call(process.argv, 2, process.argv.length);
const date = new Date();
const cwd = process.cwd();


if (!folder) {
    console.log('请指定一个路径(文件夹)用来存放处理结果')
    process.exit(1)
}

main()

async function main() {
    try {
        let { file, csvFiles } = await genCSV(stdin)


        // Gen workfolder
        let currTimeFolder = getFolderByDate(date)
        // let folder = await writeFolder();
        let workfolder = path.resolve(cwd, folder, currTimeFolder)

        if (!genFolder(workfolder)) {
            console.log('请指定一个合法的路径(文件夹)用来存放处理结果')
            process.exit(1)
        }


        // Format stylelint undos with svg, and save them.
        const stylelintErrCsvsPath = path.resolve(workfolder, stylelintErrCsvs)
        const stylelintCorrectingCsvsPath = path.resolve(workfolder, stylelintCorrectingCsvs)
        const stylelintUnReplaceAbleCsvsPath = path.resolve(workfolder, stylelintUnReplaceAbleCsvs)
        const stylelintErrUmformatPath = path.resolve(workfolder, stylelintErrUmformat)
        if (!genFolder(stylelintErrCsvsPath)) {
            console.log("Can't not create stylelintErrCsvsPath")
        }
        if (!genFolder(stylelintCorrectingCsvsPath)) {
            console.log("Can't not stylelintCorrectingCsvsPath")
        }
        if (!genFolder(stylelintUnReplaceAbleCsvsPath)) {
            console.log("Can't not stylelintUnReplaceAbleCsvsPath")
        }

        fs.writeFileSync(stylelintErrUmformatPath, file)
        console.log(`Saved stylelint errors msgs at：${stylelintErrUmformatPath},which contains ${file.length} chars.\n`);

        // Save cwd
        fs.writeFileSync(path.resolve(workfolder, 'cwd'), cwd)
        console.log(`Generated workerfolder at: ${workfolder}\n`)

        // 写入csv文件
        for (let i = 0; i < csvFiles.length; i++) {
            const csv = csvFiles[i];
            fs.writeFile(path.resolve(stylelintErrCsvsPath, csv.fileName), csv.csvStr, err => {
                if (err) {
                    console.error(err)
                }
            })
        }
    } catch (e) {
        console.log(`Saving stylelint erros failed\n`);
        console.error(e)
        process.exit(1)
    }

    console.log('Generating svgs of stylelint errors....\n')
    // await genCSV(stylelintErrUmformatPath, stylelintErrCsvsPath)
    // console.log(`Save svgs of stylelint errors at ${stylelintErrCsvsPath}.\n`)

}