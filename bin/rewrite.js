#!/usr/bin/env node

const csv = require('csv');
const fs = require('fs');
const path = require('path')
const {
    stylelintCorrectingCsvs,
    stylelintUnReplaceAbleCsvs } = require('./dstoken/conf')

const { readCSV } = require('./dstoken/read-csv.js');
const { getAnswer } = require('./dstoken/args');
const { getLine, replaceLine } = require('./dstoken/sed/index.js');


// Main
(async () => {
    const cwd = process.cwd();
    let workfolder = await getAnswer('请指定工作文件夹路径(绝对路径|相对路径)')
    let token = await getAnswer('请指定要处理的问题csv文件(不含扩展名)')
    if (!workfolder) {
        console.log('请指定工作文件夹路径')
        process.exit(1)
    }
    if (!token) {
        console.log('请指定要处理的问题')
        process.exit(1)
    }

    let absWorkfolderPath = path.resolve(cwd, workfolder)
    let tokenFilePath = path.resolve(cwd, workfolder, stylelintCorrectingCsvs, token + '.csv');
    if (!fs.existsSync(absWorkfolderPath)) {
        console.log('工作文件夹不存在')
        process.exit(1)
    }
    if (!fs.existsSync(tokenFilePath)) {
        console.log('指定要处理的问题的csv文件不存在')
        process.exit(1)
    }
    console.log('工作文件夹路径：\n', workfolder)
    console.log('要处理的问题的csv文件路径：\n', tokenFilePath)

    let inFilePathCWD = fs.readFileSync(path.resolve(cwd, workfolder, 'cwd')).toString()

    const records = await readCSV(tokenFilePath)
    if (!records) {
        process.exit(1)
    }


    console.log('进行文件替换：\n')
    const undos = []
    for (let i = 0; i < records.length; i++) {
        const record = records[i];

        if (i === 0) {
            undos.push(record)
            continue
        }

        let search = record[0] || ''
        let replace = record[2] || ''
        let targetPath = record[4] || ''
        let lineNo = record[5] || ''

        search = search.trim()
        replace = replace.trim()
        search = search.trim()

        targetPath = path.resolve(inFilePathCWD, targetPath)
        console.log(targetPath)

        if (!(search && replace && targetPath)) {
            undos.push(record)
            continue
        }


        // let data = fs.readFileSync(targetPath, 'utf8')
        // let curr = data
        // let last = curr
        // curr = last.replace(search, replace)

        // // while (curr !== last) {
        // //   last = curr
        // //   curr = last.replace(search, replace)
        // // }

        // if (curr !== data) {
        //     fs.writeFileSync(targetPath, curr)
        // } else {
        //     undos.push(record)
        // }

        /** With sed */
        let currLine = getLine(lineNo, targetPath)
        currLine = currLine.replace(/\n$/, '')
        let newLine = currLine.replace(search, replace)
        if (newLine !== currLine) {
            replaceLine(lineNo, newLine, targetPath)
        } else {
            undos.push(record)
        }

    }
    if (undos.length > 0) {
        console.log('未处理文件：\n')
        const undoFolder = path.resolve(absWorkfolderPath, stylelintUnReplaceAbleCsvs)
        try {
            fs.mkdirSync(undoFolder)
        } catch (err) {

        }

        let objdata = undos.map(one => {
            return {
                value: one[0],
                error: one[1],
                newtoken: one[2],
                desc: one[3],
                path: one[4],
            }
        })

        csv.stringify(objdata, {
            columns: [
                { key: 'value' },
                { key: "error" },
                { key: "newtoken" },
                { key: 'desc' },
                { key: 'path' }
            ]
        }, function (err, data) {
            console.log(`${undoFolder}/${path.basename(tokenFilePath, 'csv')}csv`)
            fs.writeFile(`${undoFolder}/${path.basename(tokenFilePath, 'csv')}csv`, data, err => {
                if (err) {
                    console.error(err)
                    return
                }
                //文件写入成功。
            })
        })
    }

})()