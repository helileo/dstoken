
### Install 
Please install gobally, then you will get two commands: csv and rewrite.
```shell
npm install dstoken-cli -g
````
### Usage
#### csv
```shell 
stylint path/to/file --fix | csv path/to/workerfolder
# eg.
npx stylelint './{components/event,pages/event-vertical,pages/event-live,pages/event,components/fnb,pages/fnb}/**/*.{css,vue,scss,html}' | csv  /Users/klook/data/dstoken
   
```
#### rewrite
```shell 
rewrite design-token-name path/to/workerfolder/folder_created_by_time

# eg
rewrite
```

#### backup
```shell 
# nuxtweb
    npx stylelint '/Users/klook/dt/klook-nuxt-web/{components/event,pages/event-vertical,pages/event-live,pages/event,components/fnb,pages/fnb}/**/*.{css,vue,scss,html}'

    npx stylelint '/Users/klook/leisure/demo/klook-nuxt-web/{components/event,pages/event-vertical,pages/event-live,pages/event,components/fnb,pages/fnb}/**/*.{css,vue,scss,html}'
   

# newweb
    npx stylelint '/Users/klook/new/klook-new-web/web/{s/src/desktop/scss/pages/food_and_beverage,s/src/mobile/scss/pages/fnb,s/src/desktop/js/food_and_beverage,s/src/mobile/js/fnb}/**/*.{css,vue,scss,html}'
```